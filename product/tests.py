import imp
from django.test import TestCase
from django.contrib.auth import get_user_model

from rest_framework.test import APITestCase
# Create your tests here.

from product.models import Product, Variant

User  = get_user_model()

class ProductApiTest(APITestCase):
    

    def setUp(self, *args, **kwargs):
        user = User.objects.create(username='test',email="tets@gmail.com")
        data = {
            "name": "test",
            "class_name": "tets",
            "price": 323,
            "image": None,
            "status": "available"
        }
        product = Product.objects.create(**data)
        variant_data={ "title": "Anchor", "available_stock": "60", "product":product}
        variant = Variant.objects.create(**variant_data)
        user.set_password("test@123")
        user.save()
        self.client.login(username=user.username, password="test@123")

    def test_list_product(self, *args, **kwargs):
        response = self.client.get("/product/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['data']), 1)
        self.assertEqual(response.data['data'][0]['name'], "test")
        self.client.logout()
    
    def test_status_filter(self, *args, **kwargs):
        response = self.client.get("/product/?status=unavailable")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['data']), 0)
        self.client.logout()
    
    def test_filter(self, *args, **kwargs):
        response = self.client.get("/product/?status=available")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['data']), 1)
        self.client.logout()
    
    def test_product_create(self, *args, **kwargs):
        data = {
            "name": "test",
            "class_name": "tets",
            "price": 323,
            "status": "available",
            "variants": [{ "title": "Anchor", "available_stock": "60"}]
        }
        response = self.client.post("/product/", data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {"success": True})
