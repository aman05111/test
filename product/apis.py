from rest_framework import viewsets, response, permissions

from .models import Product
from .serializers import ProductSerializer, ProductListSerializer

class ProductViewSet(viewsets.ModelViewSet):
    """
    Product crud operations
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    model = Product
    filter_fields = ('name', 'status','class_name',)
    permission_classes = (permissions.IsAuthenticated,)


    def list(self, request, *arg, **kwargs):
        """
        list all the product

        filter:
            name
            status
        /product/?name=&status=unavailable
        """
        return response.Response({
            'data': ProductListSerializer(
                instance=self.filter_queryset(self.get_queryset()),
                many=True
                ).data
        })


    def create(self, request, *args, **kwargs):
        super().create(request, *args, **kwargs)
        return response.Response({
            "success": True
        })

    def update(self, request, *args, **kwargs):
        super().update(request, *args, **kwargs)
        return response.Response({
            "success": True
        })
    
    def destroy(self, request, *args, **kwargs):
        super().destroy(request, *args, **kwargs)
        return response.Response({
            "success": True
        })