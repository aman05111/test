from pyexpat import model
from statistics import mode
from django.db import models

# Create your models here.


class Product(models.Model):
    """
    model class for product
    """

    CHOICES = (
        ("available", "Available"), 
        ("unavailable", "Unavailable"),
    )
    name = models.CharField(max_length=255)
    class_name = models.CharField(max_length=255)
    price = models.DecimalField(max_length=255, decimal_places=2, max_digits=10)
    image = models.FileField(max_length=255, null=True, blank=True)
    status = models.CharField(max_length=255, choices=CHOICES)

    class Meta:
        ordering = ["name"]


class Variant(models.Model):
    title = models.CharField(max_length=255)
    available_stock = models.IntegerField()
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, blank=True, related_name="variants")
